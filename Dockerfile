FROM php:7.4-cli
COPY time.php /
WORKDIR /
CMD [ "php", "time.php" ]
