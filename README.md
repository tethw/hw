homework for tet:

- [ ] Gitlab.com izveidot repozitoriju, kurā ir php skripts kas izvada sistēmas laiku.
- [ ] Izveidot pipeline no  2 soļiem - build un deploy.
- [ ] Izveidot jaunu CI/CD variable - CI_TEST ar vērtību 123.
- [ ] Build solī nepieciešams uzbuildot Docker image un iepušot viņu Gitlab container registry.
- [ ] Deploy solī nepieciešams: izsaukt docker info un php skriptu no tikko uzbuildotā Docker image, kā arī izvadīt CI_TEST vērtību no gitlab variables.
- [ ] Abi soļi (build un deploy) izpildās tikai develop brančā.
- [ ] Pēc uzdevuma izpildes nepieciešams mums atsūtīt piekļuvi repozitorijam
